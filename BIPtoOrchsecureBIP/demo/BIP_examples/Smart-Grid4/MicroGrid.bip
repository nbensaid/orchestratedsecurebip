/*****************************
/* Model of MicroGrid System 
/*****************************
/ The system consists of a finite number of prosumers communicating only
/ to a micro-grid component. Initially, each prosumer sends its consumption
/ and production plan (P,C,B), for the next day to the micro-grid.  Production P, 
/ consumption C and Battery B are expressed using integer energy units 
/ where  0< P <2, -3< C <0 and  -1< B <1. The SMG validates the plans received 
/ by checking that the overall energy flow through the grid implied by these 
/ plans does not exceed the power line capacity. 
/ The security requirements  consist on:
/ 	- Ensuring the confidentiality of energy consumption plan for each prosumer 
/ 	that can reveal sensitive competitive information such us it's production capacity. 
/ 	- Ensuring that no prosumer is able to deduce the consumption plan of any other prosumer
/	 by observing the received ack information.



/*****************************/
/* Definition of port types  */
/*****************************/
 model MicroGrid
 port type dataport( int x, int y, int z) //port with associated data
 port type single_dataport( int e)
 port type eport //port with  no data

/*********************************/
/* Definition of connector types */
/*********************************/
connector type SendRec(dataport s, dataport r) // strong synchronization between s, r
  define s r 
  on s r 
    up {} 
    down {r.x=s.x; r.y=s.y; r.z=s.z;}
end

connector type single_SendRec(single_dataport s, single_dataport r) // strong synchronization between s, r
  define s r 
  on s r 
    up {} 
    down {r.e=s.e;}
end

/***************************************/
/* Definition of atomic type: Prosumer1 */
/***************************************/
atomic type Prosumer1
//@security(pl="Prosumer1:SMG")
  data int P  //Energy production value
  data int C  //Energy consumption value
  data int B  //Battery consum/prod  value
  data int ack //acknowledgment value
  export port dataport H1_outplan(P,C,B)
  export port single_dataport H1_input(ack)
  port eport internal1
  port eport internal2


  place l1 
  place l2
  place l3 
  place l4
  place l5
  place l6
  place l7

  initial to l1 do {P=0;C=0;B=0; ack=0;}
  on internal1 from l1 to l2 do {P=rand(P);}
  on internal1 from l2 to l3 do {C=rand(C);}
  on internal1 from l3 to l4 do {B=rand(B);}
  on H1_outplan from l4 to l5 
  on H1_input from l5 to l6 
  on internal1 from l6 to l7 provided (ack!=0) do {C=rectify(ack);P=rectify(ack);B=rectify(ack);}
  on H1_outplan  from l7 to l1 
  on internal2  from l6 to l1 provided (ack==0)
end


/***************************************/
/* Definition of atomic type: Prosumer2 */
/***************************************/
atomic type Prosumer2
//@security(pl="Prosumer2:SMG ; Prosumer3:SMG ")
  data int P  //Energy production value
  data int C  //Energy consumption value
  data int B  //Battery consum/prod  value
  data int ack //acknowledgment value
  export port dataport H2_outplan(P,C,B)
  export port single_dataport H2_input(ack)
  port eport internal1
  port eport internal2


  place l1 
  place l2
  place l3 
  place l4
  place l5
  place l6
  place l7

  initial to l1 do {P=0;C=0;B=0; ack=0;}
  on internal1 from l1 to l2 do {P=rand(P);}
  on internal1 from l2 to l3 do {C=rand(C);}
  on internal1 from l3 to l4 do {B=rand(B);}
  on H2_outplan from l4 to l5 
  on H2_input from l5 to l6 
  on internal1 from l6 to l7 provided (ack!=0) do {C=rectify(ack);P=rectify(ack);B=rectify(ack);}
  on H2_outplan  from l7 to l1 
  on internal2  from l6 to l1 provided (ack==0)
end


/***************************************/
/* Definition of atomic type: Prosumer3 */
/***************************************/
atomic type Prosumer3
//@security(pl="Prosumer2:SMG")
  data int P  //Energy production value
  data int C  //Energy consumption value
  data int B  //Battery consum/prod  value
  data int ack //acknowledgment value
  export port dataport H3_outplan(P,C,B)
  export port  single_dataport H3_input(ack)
  port eport internal1
  port eport internal2



  place l1 
  place l2
  place l3 
  place l4
  place l5
  place l6
  place l7

  initial to l1 do {P=0;C=0;B=0; ack=0;}
  on internal1 from l1 to l2 do {P=rand(P);}
  on internal1 from l2 to l3 do {C=rand(C);}
  on internal1 from l3 to l4 do {B=rand(B);}
  on H3_outplan from l4 to l5 
  on H3_input from l5 to l6 
  on internal1 from l6 to l7 provided (ack!=0) do {C=rectify(ack);P=rectify(ack);B=rectify(ack);}
  on H3_outplan  from l7 to l1 
  on internal2  from l6 to l1 provided (ack==0)
end






/***************************************/
/* Definition of atomic type: SMG */
/***************************************/
atomic type SMG
  data int P1,P2,P3  //Energy production value
  data int C1,C2,C3  //Energy consumption value
  data int B1,B2,B3 //Battery consum/prod  value
  data int ack1,ack2,ack3 //acknowledgment value
  export port dataport H1_inplan1(P1,C1,B1)
  export port dataport H2_inplan2(P2,C2,B2)
  export port dataport H3_inplan3(P3,C3,B3)
  export port  single_dataport H1_outplan1(ack1)
  export port  single_dataport H2_outplan2(ack2)
  export port  single_dataport H3_outplan3(ack3) 
  port eport internal1
  port eport internal2
  port eport internal3



  place l1 
  place l2
  place l3 
  place l4
  place l5
  place l6
  place l7
  place l8

  initial to l1 do{P1=0;C1=0;B1=0; ack1=0; P2=0;C2=0;B2=0; ack2=0; P3=0;C3=0;B3=0; ack3=0;}
  on H1_inplan1 from l1 to l2  
  on H2_inplan2 from l2 to l3
  on H3_inplan3 from l3 to l4
  on internal1 from l4 to l5 provided ((4<(C1+C2+C3+P1+P2+P3+B1+B2+B3))&&((C1+C2+C3+P1+P2+P3+B1+B2+B3)<7))do {ack1= 0; ack2=0; ack3=0;}
  on internal2 from l4 to l5 provided((C1+C2+C3+P1+P2+P3+B1+B2+B3)>7) do {ack1= (C1+C2+C3+P1+P2+P3+B1+B2+B3)-7/4; ack2= 2*(C1+C2+C3+P1+P2+P3+B1+B2+B3)-7/4;ack3= (C1+C2+C3+P1+P2+P3+B1+B2+B3)-7/4;}
  on internal3 from l4 to l5 provided((C1+C2+C3+P1+P2+P3+B1+B2+B3)<4) do {ack1= (4-(C1+C2+C3+P1+P2+P3+B1+B2+B3))/4;ack2= 2*(4-(C1+C2+C3+P1+P2+P3+B1+B2+B3))/4;ack3= (4-(C1+C2+C3+P1+P2+P3+B1+B2+B3))/4;}
  on H1_outplan1 from l5 to l6
  on H2_outplan2 from l6 to l7
  on H3_outplan3 from l7 to l8
end


/********************************************/
/* definition of compound type: SmartGrid */
/********************************************/

compound type MicroGrid
  component Prosumer1 Pr1
  component Prosumer2 Pr2
  component Prosumer3 Pr3
  component SMG S1

 

  connector SendRec out_in_plan1(Pr1.H1_outplan, S1.H1_inplan1)
  connector SendRec out_in_plan2(Pr2.H2_outplan, S1.H2_inplan2)
  connector SendRec out_in_plan3(Pr3.H3_outplan, S1.H3_inplan3)
  connector single_SendRec out_in_ack1(S1.H1_outplan1, Pr1.H1_input)
  connector single_SendRec out_in_ack2(S1.H2_outplan2, Pr2.H2_input)
  connector single_SendRec out_in_ack3(S1.H3_outplan3, Pr3.H3_input)



end
component MicroGrid MG
end





