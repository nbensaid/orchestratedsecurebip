**************************
   BPEL2BIP Translator
   Version 1.0
   NAJAH BEN SAID/ TAMIS
**************************

This tool is a translator of BPEL files to BIP atomic components. 
The BPEL specification is BPEL2.0 from https://


A- Package details
------------------
The project contains the following files and directories:

	- demo: contains the different example systems to test

	- gen: contains the tool jar to parse and translate the BPEL files

    - README file that describes the tool



B- Prerequisites
----------------

    - Linux Ubuntu 16.04 distribution

    - Java JRE 1.6 (or higher)



C- Running the tool
------------------

To run the tool you use the command in the terminal:
 $./BPEL2BIP.sh  <input_BPEL>  <output_BIP>

