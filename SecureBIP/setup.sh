#!/bin/bash
FDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

ROOT=${FDIR}

ENGINE=${1:-reference-engine}

export PATH=$PATH:$(ls -d ${FDIR}/bin/)

echo "Environment configured for engine: " ${ENGINE}


