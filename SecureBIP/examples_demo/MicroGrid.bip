/*****************************
/* Model of MicroGrid System 
/*****************************
/ The system consists of a finite number of prosumers communicating only
/ to a micro-grid component. Initially, each prosumer sends its consumption
/ and production plan (P,C,B), for the next day to the micro-grid.  Production P, 
/ consumption C and Battery B are expressed using integer energy units 
/ where  0< P <2, -3< C <0 and  -1< B <1. The SMG validates the plans received 
/ by checking that the overall energy flow through the grid implied by these 
/ plans does not exceed the power line capacity. 
/ The security requirements  consist on:
/ 	- Ensuring the confidentiality of energy consumption plan for each prosumer 
/ 	that can reveal sensitive competitive information such us it's production capacity. 
/ 	- Ensuring that no prosumer is able to deduce the consumption plan of any other prosumer
/	 by observing the received ack information.



/*****************************/
/* Definition of port types  */
/*****************************/
 package MicroGrid
 port type dataport( int x, int y, int z) //port with associated data
 port type single_dataport( int e)
 port type eport() //port with  no data

/*********************************/
/* Definition of connector types */
/*********************************/
connector type SendRec(dataport s, dataport r) // strong synchronization between s, r
  define s r 
  on s r 
    up {} 
    down {r.x=s.x; r.y=s.y; r.z=s.z;}
end

connector type single_SendRec(single_dataport s, single_dataport r) // strong synchronization between s, r
  define s r 
  on s r 
    up {} 
    down {r.e=s.e;}
end

/***************************************/
/* Definition of atomic type: Prosumer1 */
/***************************************/
atom type Prosumer1()
@security(pl="Prosumer1:SMG")
  data int Pp  //Energy production value
  data int Cp  //Energy consumption value
  data int Bp  //Battery consum/prod  value
  data int ack //acknowledgment value
  export port dataport outplan(Pp,Cp,Bp)
  export port  single_dataport in(ack)
  port eport internal1(), internal2()


  place l1,l2,l3,l4,l5,l6,l7

  initial to l1 do {Pp=0;Cp=0;Bp=0; ack=0;}
  on internal1 from l1 to l2 do {Pp=rand(Pp);}
  on internal1 from l2 to l3 do {Cp=rand(Cp);}
  on internal1 from l3 to l4 do {Bp=rand(Bp);}
  on outplan from l4 to l5 
  on in from l5 to l6 
  on internal1 from l6 to l7 provided (ack!=0) do {Cp=rectify(ack);Pp=rectify(ack);Bp=rectify(ack);}
  on outplan  from l7 to l1 
  on internal2  from l6 to l1 provided (ack==0)
end


/***************************************/
/* Definition of atomic type: Prosumer2 */
/***************************************/
atom type Prosumer2()
@security(pl="Prosumer2:SMG ; Prosumer3:SMG ")
  data int Ppp  //Energy production value
  data int Cpp  //Energy consumption value
  data int Bpp  //Battery consum/prod  value
  data int ack //acknowledgment value
  export port dataport outplan(Ppp,Cpp,Bpp)
  export port  single_dataport in(ack)
  port eport internal1(), internal2()


  place l1,l2,l3,l4,l5,l6,l7

  initial to l1 do {Ppp=0;Cpp=0;Bpp=0; ack=0;}
  on internal1 from l1 to l2 do {Ppp=rand(Ppp);}
  on internal1 from l2 to l3 do {Cpp=rand(Cpp);}
  on internal1 from l3 to l4 do {Bpp=rand(Bpp);}
  on outplan from l4 to l5 
  on in from l5 to l6 
  on internal1 from l6 to l7 provided (ack!=0) do {Cpp=rectify(ack);Ppp=rectify(ack);Bpp=rectify(ack);}
  on outplan  from l7 to l1 
  on internal2  from l6 to l1 provided (ack==0)
end


/***************************************/
/* Definition of atomic type: Prosumer3 */
/***************************************/
atom type Prosumer3()
@security(pl="Prosumer2:SMG")
  data int Pppp  //Energy production value
  data int Cppp  //Energy consumption value
  data int Bppp  //Battery consum/prod  value
  data int ack //acknowledgment value
  export port dataport outplan(Pppp,Cppp,Bppp)
  export port  single_dataport in(ack)
  port eport internal1(), internal2()


  place l1,l2,l3,l4,l5,l6,l7

  initial to l1 do {Pppp=0;Cppp=0;Bppp=0; ack=0;}
  on internal1 from l1 to l2 do {Pppp=rand(Pppp);}
  on internal1 from l2 to l3 do {Cppp=rand(Cppp);}
  on internal1 from l3 to l4 do {Bppp=rand(Bppp);}
  on outplan from l4 to l5 
  on in from l5 to l6 
  on internal1 from l6 to l7 provided (ack!=0) do {Cppp=rectify(ack);Pppp=rectify(ack);Bppp=rectify(ack);}
  on outplan  from l7 to l1 
  on internal2  from l6 to l1 provided (ack==0)
end

/***************************************/
/* Definition of atomic type: SMG */
/***************************************/
atom type SMG()
  data int P1,P2,P3  //Energy production value
  data int C1,C2,C3  //Energy consumption value
  data int B1,B2,B3 //Battery consum/prod  value
  data int ack1,ack2,ack3 //acknowledgment value
  export port dataport inplan1(P1,C1,B1)
  export port dataport inplan2(P2,C2,B2)
  export port dataport inplan3(P3,C3,B3)
  export port  single_dataport outplan1(ack1)
  export port  single_dataport outplan2(ack2)
  export port  single_dataport outplan3(ack3) 
  port eport internal1(), internal2(), internal3() 


  place l1,l2,l3,l4,l5,l6,l7,l8

  initial to l1 do{P1=0;C1=0;B1=0; ack1=0; P2=0;C2=0;B2=0; ack2=0; P3=0;C3=0;B3=0; ack3=0;}
  on inplan1 from l1 to l2  
  on inplan2 from l2 to l3
  on inplan3 from l3 to l4
  on internal1 from l4 to l5 provided ((4<(C1+C2+C3+P1+P2+P3+B1+B2+B3))&&((C1+C2+C3+P1+P2+P3+B1+B2+B3)<7))do {ack1= 0; ack2=0; ack3=0;}
  on internal2 from l4 to l5 provided((C1+C2+C3+P1+P2+P3+B1+B2+B3)>7) do {ack1= (C1+C2+C3+P1+P2+P3+B1+B2+B3)-7/4; ack2= 2*(C1+C2+C3+P1+P2+P3+B1+B2+B3)-7/4;ack3= (C1+C2+C3+P1+P2+P3+B1+B2+B3)-7/4;}
  on internal3 from l4 to l5 provided((C1+C2+C3+P1+P2+P3+B1+B2+B3)<4) do {ack1= (4-(C1+C2+C3+P1+P2+P3+B1+B2+B3))/4;ack2= 2*(4-(C1+C2+C3+P1+P2+P3+B1+B2+B3))/4;ack3= (4-(C1+C2+C3+P1+P2+P3+B1+B2+B3))/4;}
  on outplan1 from l5 to l6
  on outplan2 from l6 to l7
  on outplan3 from l7 to l8
end



/********************************************/
/* definition of compound type: SmartGrid */
/********************************************/

compound type MicroGrid()
  component Prosumer1 Pr1()
  component Prosumer2 Pr2()
  component Prosumer3 Pr3()
  component SMG S()

  connector SendRec out_in_plan1(Pr1.outplan, S.inplan1)
  connector SendRec out_in_plan2(Pr2.outplan, S.inplan2)
  connector SendRec out_in_plan3(Pr3.outplan, S.inplan3)
  connector single_SendRec out_in_ack1(S.outplan1, Pr1.in)
  connector single_SendRec out_in_ack2(S.outplan2, Pr2.in)
  connector single_SendRec out_in_ack3(S.outplan3, Pr3.in)
end

end





